package listasecircular;
//proyecto de lista circular
 
import javax.swing.JOptionPane;
//importamos la clase  tipo opccion

class CNodo {
    // iniciamos clase CNodo
	int dato;// creamos una variable tipo entera llamada dato
	CNodo siguiente;// creamos una variable tipo CNodo llamada siguiente
        
	public CNodo()	{//
            siguiente = null;//se le asigana null a la variable siguiente
	}
}

class CLista {//iniciamos la clase Clista
    CNodo cabeza;//creamos una variable cabeza tipo CNodo
    public CLista()	{
            cabeza = null;//le asignamos null a cabeza
    }

    public void InsertarDato(int dat) {// creamos un metodo que permita inserta un dato
        CNodo NuevoNodo;//creamos un nuevo nodo
        CNodo antes, luego;// el nuevo nodo apunta a antes y luego
        NuevoNodo = new CNodo();//define nuevo nodo
        NuevoNodo.dato=dat;//agrega el nuevo nodo
        int ban=0;//creamos una bandera y le asignamos cero
        if (Vacia()){//creamos un condiccion que pregunat si la lista esta vacia
            NuevoNodo.siguiente=NuevoNodo;//incia a gregando al nuevo nodo
            cabeza = NuevoNodo;//el nuevo nodo pasa a ser cabeza
        }
        else {  if (dat<cabeza.dato) {//preguntamos si dat es igual a cabeza.dat
                    luego=cabeza;
                    antes=cabeza;
                        do{// creamos un ciclo con do 
                          antes=luego;
                          luego=luego.siguiente;
                        }while(luego!=cabeza);//pregunta si luego es igual a cabeza
                        antes.siguiente=NuevoNodo;
                        NuevoNodo.siguiente=cabeza;
                        cabeza = NuevoNodo;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
                }
                else {  antes=cabeza;
                        luego=cabeza;
                        while (ban==0){ // pregunta si bandera es igual a 0
                            if (dat>=luego.dato) {//pregunta si dat es mayor a luego 
                                antes=luego;
                                luego=luego.siguiente;
                            }
                            if (luego==cabeza){//pregunta si luego es igual a cabeza
                                ban=1;//se le asigna uno a bandera
                            }
                            else {
                                    if (dat<luego.dato){//pregunta si dat es menor a luego.dato
                                        ban=1;//se le asigna uno a bandera
                                    }
                            }
                        }
                        antes.siguiente=NuevoNodo;
                        NuevoNodo.siguiente=luego;
                }
        }
    }
    public void EliminarDato(int dat) {//creamos un metodo que permita eliminar los valores ingresados
        CNodo antes,luego;// creamos un antes y luego tipo Cnodo
        int ban=0;// cremos una variable llamada bandera asignandole cero
        if (Vacia()) {   //pregunta si esta vacia
            System.out.print("Lista vacía ");//muestra que la lista esta vacia
        }
        else {  if (dat<cabeza.dato) {//pregunta si dat es igual a cabeza.dato
                    System.out.print("dato no existe en la lista ");// muestra el mensaje
                }
                else {
                        if (dat==cabeza.dato) {//pregunta dat es igual a cabeza.dato
                            luego=cabeza;
                            antes=cabeza;
                                while(ban==0){// pregunta si bandera es igual a cero
                                    if(luego.siguiente==cabeza){//pregunta si luego.siguiente es igual a cabeza
                                        ban=1;// bandera ya no esta asignada a cero sino que paso a uno
                                    }
                                  antes=luego;
                                  luego=luego.siguiente;
                                }
                                    if(luego.siguiente==cabeza){//pregunta si luego.siguiente es igual a cabeza
                                      cabeza=null;  
                                    }
                                    else{
                                        cabeza=cabeza.siguiente;
                                         antes.siguiente=cabeza;
                                    }
                        }
                        else {  antes=cabeza;
                                luego=cabeza;
                                while (ban==0) {//pregunta si bandera es igual a cero
                                    if (dat>luego.dato) {//pregunta si dat es mayor a luego.dato
                                        antes=luego;
                                        luego=luego.siguiente;
                                    }
                                    else ban=1;//bandera no es igual a cero y pasa a ser uno
                                    if (luego==cabeza) {// pregunta si luego es igual a cabeza
                                        ban=1;//se le asigna uno a bandera
                                    }
                                    else {
                                            if (luego.dato==dat) //pregunta si luego.dato es igual a dat
                                                ban=1;//se le asigna uno a bandera 
                                    }
                                }
                                if (luego==cabeza) {//pregunta si luego es igual a cabeza
                                    System.out.print("dato no existe en la Lista ");//muestra el mensaje
                                }
                                else {
                                        if (dat==luego.dato) {//pregunta si dat es igual a luego.dato
                                            antes.siguiente=luego.siguiente;
                                        }
                                        else //si no muestra el mensaje 
                                            System.out.print("dato no existe en la Lista ");
                                }
                        }
                }
        }
    }

  public boolean Vacia() {//
        return(cabeza==null);//
    }

    public void VisualizarDatos() {//creamos un visualizador de datos que permita mostrar los datos insertados en la lista
        CNodo Temporal;// creamos una variable temporal tipo CNodo
        Temporal=cabeza;//
        if (!Vacia()) {//pregunta  si la lista esta vacia
            do{// creamos un ciclo do
             JOptionPane.showMessageDialog (null," " + Temporal.dato +" ");//imprime los datos insertados
                System.out.print( "" + Temporal.dato +" ");//imprime los datos insertados
                Temporal = Temporal.siguiente;// muestra el siguiente dato
            }while(Temporal!=cabeza);//pregunta temporal es igual a cabeza
                System.out.println("");
        }
        else
            //JOptionPane.showMessageDialog(null,"Lista vacía");
            System.out.println("Lista vacia");//manda un mensaje cuando la lista esta vacia
    }
}
public class ListaSECircularPilozo {// esto permite guardar los valores ingresados
    
    public static void main(String args[]) {//metodo que tiene valor de retorno
        //esto permite insertar datos de manera estatica
        /*CLista objLista= new CLista();
        System.out.println("Lista Original:");
        objLista.InsertarDato(7);
        objLista.InsertarDato(9);
        objLista.InsertarDato(5);
        objLista.VisualizarDatos();
        System.out.println("\nLista Sin el 7: ");
        objLista.EliminarDato(7);
        objLista.VisualizarDatos();
    }
    
}*/

        int dato;//creamos una variable dato tipo entera
       
            
        CLista lista = new CLista();//define una nueva lista
       
       do {// iniciamos un ciclo do
            //creamos un menu que nos permita ingresar los valores
            
          dato = Integer.parseInt(JOptionPane.showInputDialog("Menu de ListaSECircular\n1. Ingresar Elemento\n" + "2. Mostrar Elemento\n" + "3. Eliminar Elemento\n" + "0. Salir"));
            switch (dato) {
                case 1://creamos la opcion que perrmita insertar un dato
                   dato=Integer.parseInt(JOptionPane.showInputDialog(null,"Ingresa el elemento"));
                    JOptionPane.showMessageDialog(null,"El Elemento fue ingresado " + "" + dato);
                    lista.InsertarDato(dato);
                    break;
                case 2://muestra los elemnetos ingresados
                    JOptionPane.showMessageDialog(null,"Los Elemento ingresado son " );
                    System.out.println("Lista Original ");
                    lista.VisualizarDatos();
                    break;
                case 3:// esta opcion permite eliminar un elemento
                    dato=Integer.parseInt(JOptionPane.showInputDialog(null,"Ingresa el elemento a eliminar"));
                    JOptionPane.showMessageDialog(null,"El Elemento fue eliminado" + "" + dato);
                     System.out.println("Elemento elimnado " + "" + dato);
                    lista.EliminarDato(dato);
            }
                                         
        } while (dato != 0);//
                }
}
